package com.spring.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class TablePK implements Serializable {


    @Column(name = "month")
    private Integer month;

    @Column(name = "name")
    private String name;
}
