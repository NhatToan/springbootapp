package com.spring.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@javax.persistence.Table(name = "data_old_table")
public class Table {

    @Id
    private TablePK id;

    @Column(name = "value")
    private String value;
}
