package com.spring.model;

public class topic {

    private String id;
    private String name;
    private String desription;

    public topic(String id, String name, String desription) {
        this.id = id;
        this.name = name;
        this.desription = desription;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesription() {
        return desription;
    }
}
