package com.spring.model;

import lombok.Data;

@Data
public class TableCsv {

    private String name;

    private String value;

    private static final String COMMA = ",";
    private static final String INVERTED_COMMA = "\"";

    public TableCsv(Object[] objects) {
        this.name = (String) objects[0];
        this.value = (String) objects[1];
    }

    public String toCsvString() {
        StringBuilder sb = new StringBuilder();
        if (name != null) {
            sb.append(INVERTED_COMMA).append(name).append(INVERTED_COMMA).append(COMMA);
        }
        if (value != null) {
            sb.append(INVERTED_COMMA).append(value).append(INVERTED_COMMA);
        }

        return sb.toString();
    }
}
