package com.spring.model;

import lombok.Data;

@Data
public class ApiError {
    private String error;

    private String defaultMessage;

    public ApiError(String error, String defaultMessage) {
        this.error = error;
        this.defaultMessage = defaultMessage;
    }
}
