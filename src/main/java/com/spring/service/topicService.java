package com.spring.service;

import com.spring.model.topic;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class topicService {

    private List<topic> topics = Arrays.asList(
            new topic("1", "Music", "Relax"),
            new topic("2", "Food", "Delicious"),
            new topic("3", "Film", "Good")
        );

    public List<topic> getAllTopics(){
        return topics;
    }

    public topic getTopic(String id){
        return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
    }
}
