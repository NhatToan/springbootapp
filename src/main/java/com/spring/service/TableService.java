package com.spring.service;

import com.spring.model.TableCsv;
import com.spring.repository.TableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TableService {

    @Autowired
    TableRepository tableRepository;

    private static final String NEW_LINE = "\r\n";
    private static final String COMMA = ",";
    private static final String INVERTED_COMMA = "\"";

    public static final String[] TABLE_HEADER = {"Name", "Value"};

    public List<Object[]> getTableCsv(Integer month, Integer option) {
        List<Object[]> listTableCsv = new ArrayList<Object[]>();

        if (!StringUtils.isEmpty(month) && !StringUtils.isEmpty(option)) {
            switch (option){
                // Get all
                case 0:
                    listTableCsv = tableRepository.getCsv(month);
                    break;
                // Get only salary
                case 1:
                    listTableCsv = tableRepository.getTableCsvBySalary(month);
                    break;
            }
        }

        return listTableCsv;
    }

    public String outputCsv(List<Object[]> list) {
        List<TableCsv> listTableCsv = new ArrayList<TableCsv>();

        if (list.isEmpty()) {
            return "";
        }

        listTableCsv = list.stream().map(TableCsv::new).collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        for (String header : TABLE_HEADER) {
            sb.append(INVERTED_COMMA).append(header).append(INVERTED_COMMA).append(COMMA);
        }

        if (sb.length() > 0) {
            sb.setLength(sb.length()-1);
        }
        sb.append(NEW_LINE);
        listTableCsv.forEach(data -> sb.append(data.toCsvString()).append(NEW_LINE));

        return sb.toString();

    }
}
