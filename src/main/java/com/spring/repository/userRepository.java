package com.spring.repository;

import com.spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface userRepository extends JpaRepository<User, String> {

    @Query(value = "select * from profile_user where username=:username AND password=:password", nativeQuery = true)
    User getUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query(value = "select * from profile_user where username=:username", nativeQuery = true)
    User getUserByUsername(@Param("username") String username);
}
