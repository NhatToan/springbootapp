package com.spring.repository;

import com.spring.model.Table;
import com.spring.model.TablePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TableRepository extends JpaRepository<Table, TablePK> {

    @Query(value = "select * from data_old_table where month=:month", nativeQuery = true)
    List<Object[]> getMonth(@Param("month") Integer month);

    @Query(value = "select name, value from data_old_table where month= :month", nativeQuery = true)
    List<Object[]> getCsv(@Param("month") Integer month);

    @Query(value = "" +
            "select name, value " +
            "from data_old_table " +
            "where month= :month and name='salary' ", nativeQuery = true)
    List<Object[]> getTableCsvBySalary(@Param("month") Integer month);

}
