package com.spring.controller;

import com.spring.model.User;
import com.spring.repository.userRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
public class userController {

    final static Logger logger = LoggerFactory.getLogger(userController.class);

    @Autowired
    private userRepository userRepository;

    /**
     * Register user
     * @param user
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> Register(@RequestBody User user) {
        int result = 0;
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        try {
            User checkUser = userRepository.getUserByUsername(user.getUsername());

            if (user.getUsername().isEmpty() || user.getPassword().isEmpty()){
                logger.info("Require username and password !!!");
                result = 1;
                return new ResponseEntity<Object>(result, HttpStatus.BAD_REQUEST);
            }
            else if(checkUser != null){
                logger.info("Username already exists");
                result = 2;
                return new ResponseEntity<Object>(result, HttpStatus.BAD_REQUEST);
            }
            else {
                user.setCreateDate(currentTime);
                user.setUpdateDate(currentTime);
                userRepository.save(user);
                logger.info("Register success!!!!");
                return new ResponseEntity<Object>(result, HttpStatus.OK);
            }
        }
        catch (Exception e){
            logger.info("Error message: "+ e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            logger.info("*** Register User ***");
        }
    }

    /**
     * Login
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public ResponseEntity<Object> Login(@RequestParam String username, String password) {
        int result = 0;
        try {
            User user = userRepository.getUserByUsernameAndPassword(username, password);
            if (user != null) {
                logger.info("Login success !!");
                return new ResponseEntity<Object>(result, HttpStatus.OK);
            } else {
                logger.info("Login fail !");
                result = 1;
                return new ResponseEntity<Object>(result, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            logger.error("***Login error***", e);
            return new ResponseEntity<>(e.getStackTrace(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            logger.info("*** Check Login ***");
        }
    }

    /**
     * Change password
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/change_password", method = RequestMethod.POST)
    public ResponseEntity<Object> ChangePassword(@RequestParam String username, String password) {
        try {
            Timestamp currentTime = new Timestamp(System.currentTimeMillis());
            User user = new User();
            user = userRepository.getUserByUsername(username);
            if (StringUtils.isEmpty(user)) {
                logger.info("Username does not exist !");
                return new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);
            }
            else {
                User ChangePass = new User();
                ChangePass.setUsername(username);
                ChangePass.setPassword(password);
                ChangePass.setCreateDate(user.getCreateDate());
                ChangePass.setUpdateDate(currentTime);
                userRepository.save(ChangePass);
                logger.info("Change password !");
                return new ResponseEntity<>(user, HttpStatus.OK);
            }

        } catch (Exception e){
            logger.info("*** Change password fail !!", e);
            return new ResponseEntity<Object>(e.getStackTrace(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            logger.info("*** Change password ! ***");
        }
    }

}




