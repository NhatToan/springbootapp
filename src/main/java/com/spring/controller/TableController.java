package com.spring.controller;

import com.spring.model.ApiError;
import com.spring.model.Table;
import com.spring.repository.TableRepository;
import com.spring.service.TableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestController
public class TableController {

    final static Logger logger = LoggerFactory.getLogger(userController.class);

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TableService tableService;


    /**
     * Register data of Table
     * @param listTable
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public ResponseEntity<Object> dataPost(@RequestBody List<Table> listTable){
        try {
            int result = 0;
            for (Table table : listTable){
                if (table.getValue().isEmpty()){
                    logger.info("Value is null");
                    result = 1;
                }
            }

            if (result == 1) {
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
            else {
                for (Table t : listTable){
                    tableRepository.save(t);
                }
                logger.info("Register success !");
            }
            return new ResponseEntity<Object>(result, HttpStatus.OK);
        }
        catch (Exception e){
            logger.info("Error message: "+ e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            logger.info("*** Register TableService ***");
        }
    }

    /**
     * Get data of Table
     * @param month
     */
    @RequestMapping(value = "/table/{month}", method = RequestMethod.GET)
    public ResponseEntity<Object> dataGet(@PathVariable Integer month){
        try {
            int result = 0;
            if (StringUtils.isEmpty(month)) {
                logger.info("Value of month is null");
                result = 1;
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            List<Object[]> tables = new ArrayList<Object[]>();
            tables = tableRepository.getMonth(month);
            logger.info("data GET success!!!!");
            return new ResponseEntity<Object>(tables, HttpStatus.OK);
        }
        catch (Exception e){
            logger.info("Error message: "+e.getMessage());
            return new ResponseEntity<>(e.getStackTrace(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            logger.info("*** Get data of TableService *** ");
        }
    }

    /**
     * Export Csv of Table
     * @param month
     * @param option
     * @return
     */
    @RequestMapping(value = "/table/output/csv/{month}", method = RequestMethod.GET)
    public ResponseEntity<Object> getTableCsv(@PathVariable Integer month,
                                              @RequestParam Integer option) {

        logger.info("*** get Table output Csv ***");
        logger.info("month: " + month);
        logger.info("*** ********* ***");

        // Check parameter {month}
        if (StringUtils.isEmpty(month)) {
            ApiError error = new ApiError(null, messageSource.getMessage("message.error.table.parameter.month", null, Locale.ENGLISH));

            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        List<Object[]> list = tableService.getTableCsv(month, option);

        String csv = tableService.outputCsv(list);

        try {
            ByteArrayResource resource = new ByteArrayResource(csv.getBytes("MS932"));
            return ResponseEntity
                    .ok()
                    .contentLength(resource.contentLength())
                    .contentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
                    .body(resource);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
