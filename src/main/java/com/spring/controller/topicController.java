package com.spring.controller;

import com.spring.model.topic;
import com.spring.service.topicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class topicController {

    @Autowired
    private topicService topicService;

    final static Logger logger = LoggerFactory.getLogger(topicController.class);
    @RequestMapping("/topics")
    public List<topic> getAllTopics(){

        return topicService.getAllTopics();
    }

    @RequestMapping(value = "/topics/{id}")
    public topic getTopic(@PathVariable String id){
        logger.info("Get Topic Success !!!");
        return topicService.getTopic(id);
    }

}
